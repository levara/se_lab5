import pygame
import random

#pygame.init()

WIDTH = 1024
HEIGHT = 600
size = (WIDTH, HEIGHT)

#definiranje boja
WHITE = ( 255, 255, 255 )
BLUE  = (   0,   0, 255 )
BLACK = (   0,   0,   0 )
GREEN = (   0, 255,   0 )
RED   = ( 255,   0,   0 )
ORANGE= ( 255, 255,   0 )

#renderiranje pozdravnog teksta
pygame.font.init()
myfont = pygame.font.SysFont('Arial', 30)
welcome_text = myfont.render("Pero i djuro", False, BLACK)

welcome_image = pygame.image.load("front.jpg")
welcome_image = pygame.transform.scale(welcome_image, 
                                       (WIDTH, HEIGHT) )
hamster = pygame.image.load("hamster.png")
hamster = pygame.transform.scale(hamster, (100,100) )


#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)

#Postavlja naziv prozora
pygame.display.set_caption("Nasa kul igra")

clock = pygame.time.Clock()
hamster_time = 2000

game_state = "welcome"  # welcome/game/end
done = False
hamsterx = 100
hamstery = 100
hit = False
score = 0

while not done:
    #event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                game_state = "game"
            if event.key == pygame.K_RETURN:
                game_state = "end"
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if game_state == "game":
                click_position = event.pos
                if hamster_position.collidepoint(
                                        click_position):
                    hit = True

    if game_state == "welcome":
        screen.fill( RED )
        screen.blit(welcome_image, (0,0))
        screen.blit( welcome_text, (100, 100) )
    elif game_state == "game":
        hamster_time = hamster_time - clock.get_time()
        if hamster_time <= 0:
            game_state = "end"


        if hit:
            score += 1
            hamsterx = random.randint(100,WIDTH-100)
            hamstery = random.randint(100,HEIGHT-100)
            hamster_time = 5000
            hit = False

        screen.fill(BLUE)
        score_text = myfont.render("Score: "+str(score), 
                                        True, ORANGE)
        screen.blit(score_text, (50, 50) )
        hamster_position = screen.blit( hamster, 
                                    (hamsterx, hamstery) )
    elif game_state == "end":
        screen.fill( ORANGE )



    pygame.display.flip()

    #postavljamo fps na 60hz
    clock.tick(60)

pygame.quit()








